package com.example.redcan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        TextView btn = findViewById(R.id.textViewSignUp);
        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });
        TextView tb = findViewById(R.id.Forgotpassword);
        tb.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                startActivity(new Intent(LoginActivity.this,forgotPassoword.class));
            }
        });
        Button bt = findViewById(R.id.button);
        bt.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                startActivity(new Intent(LoginActivity.this,OtpVerification.class));
            }
        });
    }
}