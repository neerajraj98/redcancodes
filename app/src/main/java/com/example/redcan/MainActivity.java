package com.example.redcan;

import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.chaos.view.PinView;


public class MainActivity extends AppCompatActivity {
    //Variables
    Animation topAnim,bottomAnim;
    ImageView image;
    TextView logo, slogan;
    private PinView pinView;
    // private static final int SPLASH_TIME_OUT =4000;
     @Override
      protected void onCreate(Bundle savedInstanceState){
         super.onCreate(savedInstanceState);
         getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
         setContentView(R.layout.activity_main);
         //Animations
         topAnim = AnimationUtils.loadAnimation(this,R.anim.top_anim);
         bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_anim);

         //hooks
         image = findViewById(R.id.imageView1);
         logo = findViewById(R.id.textView2);
         slogan = findViewById(R.id.textView3);

         image.setAnimation(topAnim);
         logo.setAnimation(bottomAnim);
         slogan.setAnimation(bottomAnim);
         pinView = findViewById(R.id.pinView2);

    }




}




